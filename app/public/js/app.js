$(document).ready(function () {
    var gameIsRunning = false;
    var intervalId;
    function playGame() {
        var formData=new FormData();
        fetch('/nextRound', {
            method: 'post'
        }).then((response) => response.json())
            .then((jsonResponse) => {
                if (jsonResponse['status'] === 'success') {
                    $('div#field').show();
                    $('div#field').html('');
                    $('div#log').append('<p class="text-center">'+jsonResponse['message']+'</p>')
                    jsonResponse['data'].forEach(function ( player) {

                            $('div#field').append(`
                                <div class="player" id="`+player['id']+`">
                                  <div class="percent">
                                    <svg>
                                      <circle cx="105" cy="105" r="100"></circle>
                                      <circle cx="105" cy="105" r="100" style="--percent: `+player['health']+`"></circle>
                                    </svg>
                                    <div class="number">
                                      <h3>`+player['health']+`<span>%</span></h3>
                                    </div>
                                  </div>
                                  <div class="title">
                                    <h2>Player: `+player['id']+` </h2>
                                  </div>
                                </div>
                   
                            `);
                        if (jsonResponse['data'].length ==1){
                            clearInterval(intervalId);
                            $('div#status').html('<h2 class="text-center">Player '+$('.player').prop('id')+' has won the game</h2>' +
                                '<p class="alert alert-info text-center">new game Starting in 4 seconds</p>')
                            setTimeout(function() {
                                newGame();
                            }, 4000);
                        }

                    })

                }
            });
    }

    var newGame = function (){
            function randomIntFromInterval(min, max) { // min and max included
                return Math.floor(Math.random() * (max - min + 1) + min)
            }

            const rndInt = randomIntFromInterval(2, 5);
            var formData=new FormData();
            formData.append('numPlayers',rndInt);
            fetch('/newgame', {
                method: 'post',
                body: formData,
            }).then((response) => response.json())
                .then((jsonResponse) => {
                    if (jsonResponse['status'] === 'success') {
                        gameIsRunning=true;
                        $('div#field').show();
                        $('div#field').html('');
                        $('div#status').html('');
                        $('div#log').html('');
                        $('div#status').html('<h2 class="text-center">'+jsonResponse['message']+'</h2>')
                        jsonResponse['data'].forEach(function ( player) {
                                $('div#field').append(`
                                <div class="player">
                                  <div class="percent">
                                    <svg>
                                      <circle cx="105" cy="105" r="100"></circle>
                                      <circle cx="105" cy="105" r="100" style="--percent: `+player['health']+`;"></circle>
                                    </svg>
                                    <div class="number">
                                      <h3>`+player['health']+`<span>%</span></h3>
                                    </div>
                                  </div>
                                  <div class="title">
                                    <h2>Player: `+player['id']+` </h2>
                                  </div>
                                </div>
                   
                            `);
                        })
                        intervalId = window.setInterval(function(){
                            // call your function here
                            playGame();
                        }, 300);
                    }
                });


    }
    newGame();


});
