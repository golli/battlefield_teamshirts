<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Welcome to battlefield_teamshirts </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="public/css/app.css">
    <script src="public/js/bin/jquery/jquery-3.6.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="public/js/app.js"></script>
</head>

<body>

<header class="bg-dark py-5">
    <div class="container px-4 px-lg-5 my-5 text-white">
        <div class="text-center ">
            <h1 class="display-4 fw-bolder">Knights game</h1>
        </div>
    </div>
</header>
<section class="my-5">
    <div id="status"></div>
    <div class="container px-4 px-lg-5 mt-5 text-center">
        <div id="field" style="display: none" class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

        </div>

    </div>
    <div id="log"></div>
</section>
</body>
</html>