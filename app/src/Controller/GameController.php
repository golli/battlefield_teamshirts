<?php

namespace App\Controller;

use App\Core\JsonResponse;
use App\Core\Request;
use App\Core\SessionManager;
use App\Model\Field;

class GameController
{
    private SessionManager $session;

    public function __construct()
    {
        $this->session = new SessionManager();
    }

    /**
     *
     * creates a new game with n players
     * @return JsonResponse
     */
    public function createNewGame(): JsonResponse
    {

        $numberOfPlayers = Request::getRequest('numPlayers');
        $field = new Field($numberOfPlayers);
        $this->session->set('field', $field);
        $this->session->set('playerAttackingId', 0);
        $message = 'Game Started with ' . $numberOfPlayers . ' Players';
        return new JsonResponse('success', $message, $field->getActivePlayers());
    }

    /**
     * return the details of the round
     * @return JsonResponse
     */
    public function nextRound(): JsonResponse
    {
        $field = $this->session->get('field');
        $playerAttacking = $this->session->get('playerAttackingId');
        if (empty($field)) {
            $message = 'No Game Found';
            return new JsonResponse('error', $message);
        } else {

            $roundDetails = $field->nextFight($playerAttacking);
            $players=$field->getActivePlayers();
            if (count($players) == 1){
                return new JsonResponse('success', 'Game Ended', $players);
            }
            $message = 'Player: ' . $roundDetails['playerAttacking'] . ' dealt ' . $roundDetails['damage'] . ' damage to Player: ' . $roundDetails['playerDefending'];
            return new JsonResponse('success', $message, $players);


        }

    }


}