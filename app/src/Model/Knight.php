<?php

namespace App\Model;

class Knight
{
    private int $health;

    /**
     * @param int $initialHealth
     */
    function __construct(int $initialHealth=100)
    {
        $this->health = $initialHealth;
    }


    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    public function setHealth(int $health): void
    {
        $this->health = $health;
    }

    /**
     * check if knight is dead
     * @return bool
     */
    public function isDead(): bool
    {
        return $this->health <= 0;
    }

}