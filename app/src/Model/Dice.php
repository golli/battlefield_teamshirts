<?php

namespace App\Model;

class Dice
{
    /**
     * roll a dice
     * @return int between 1 - 6
     */
    public static function roll(): int
    {
        return rand(1,6);
    }

}