# battlefield_teamshirts

## Usage

Start the Docker container:

    docker compose up -d

App should be running on http://localhost:8000/

## Description
An online game running PHP on serverSide and Javascript on the frontEnd

Project Structure 
============================


### The structure for our web App

    .
    ├── public                  # contains style and javascript files
    ├── src                     # Source files, PHP code.
    │   ├── Controller          # Controllers to return View and JsonData(API) 
    │   ├── Core                # contains custom classes to hundel json response with HTTP status, request and session
    │   └── Model               # contains our data Objects 
    ├── vendor                  # PSR4 Autoloading your PHP files using Composer
    ├── .htaccess               rewrite calls to index.php
    └── README.md

we have 3 Controllers:
  > GameController: our API endpoint for running a new game and executing every round
  > RoutesController: a small file for handling our routing system
  > IndexController: to serve our Home page Interface
  
one Javascript file to call our APIs:

  >http://localhost:8000/newgame
  
  >http://localhost:8000/nextRound


### Notes

usually Vendor folder is not included within git files, you just need to run to have them
       
       composer install 
